using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Processamento.Modelo.Entidade;
using Processamento.Modelo.Negocio;

namespace Processamento.Util
{
	class Logger
	{
		private LogProcesso lp = new LogProcesso(LoggerSettings.contexto);
		private LOG_PROCESSO o = new LOG_PROCESSO();

		public string nomeMetodo { get; set; }
		public DateTime dataAtual { get; set; }


		public Logger() {
		}

		public void info(string msg)
		{
			logar(nomeMetodo, msg, "INFO");
		}

		public void debug(string msg)
		{
			if (LoggerSettings.debugar)
				logar(nomeMetodo, msg, "DEBUG");
		}

		public void error(string msg)
		{
			logar(nomeMetodo, msg, "ERRO");
		}

		private void logar(string rotina, string msg, string tipo)
		{
			o.data_log = DateTime.Now;
			o.data_atual = DateTime.Now;
			o.nome_rotina = rotina;
			o.mensagem = msg;
			o.tipo = tipo;

			lp.salvar(o);

			if (LoggerSettings.usarConsole)
			{
				Console.WriteLine(o.data_log.ToString("yyyy-MM-dd HH:mm:ss.fffffff") + "   " + o.tipo + "   " + o.nome_rotina + "   " + o.mensagem);
			}
		}
	}

	class LoggerSettings
	{
		private static Contexto cntxt;
		public static Contexto contexto { get { return cntxt; } }
		public static bool debugar { get; set; }
		public static bool usarConsole { get; set; }
		static LoggerSettings()
		{
			cntxt = new Contexto();
			debugar = false;
			usarConsole = false;
		}

		private LoggerSettings() { }
	}
}
