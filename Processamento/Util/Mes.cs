﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processamento.Util
{
	class Mes
	{
		private Mes() { }

		public static int nomeMesParaInt(string nome)
		{
			int mes = 0;

			switch (nome)
			{
				case "JANEIRO":
					mes = 1;
					break;
				case "FEVEREIRO":
					mes = 2;
					break;
				case "MARCO":
					mes = 3;
					break;
				case "ABRIL":
					mes = 4;
					break;
				case "MAIO":
					mes = 5;
					break;
				case "JUNHO":
					mes = 6;
					break;
				case "JULHO":
					mes = 7;
					break;
				case "AGOSTO":
					mes = 8;
					break;
				case "SETEMBRO":
					mes = 9;
					break;
				case "OUTUBRO":
					mes = 10;
					break;
				case "NOVEMBRO":
					mes = 11;
					break;
				case "DEZEMBRO":
					mes = 12;
					break;
			}

			return mes;
		}

	}
}
