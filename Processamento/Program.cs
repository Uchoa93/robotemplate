using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Processamento.Modelo.Negocio;
using Processamento.Modelo.Entidade;
using Processamento.Excel;
using Processamento.VO;
using Processamento.Util;

namespace Processamento
{
	class Program
	{
		static void Main(string[] args)
		{
			DateTime hoje = DateTime.Today;
			Contexto contexto = new Contexto();
			SqlParameter[] sqlParams;

			LoggerSettings.usarConsole = true;

			Logger logger = new Logger();
			logger.nomeMetodo = "Processamento.Main";
			logger.dataAtual = hoje;

			List<OPERADORA> lista = new List<OPERADORA>();
			PROCESSO processo = new PROCESSO();

			Arquivo arquivo = new Arquivo();

			Processamento.Modelo.Negocio.Processamento procn = new Processamento.Modelo.Negocio.Processamento();
			int idProcessamento = 0;

			bool jaExecutou;

			try
			{
				logger.info("Início do Processamento.");

				logger.info("Data Atual: " + hoje.ToString("yyyy-MM-dd"));

				lista = (new Operadora()).obterOperadoras("0");
				Processo pn = new Processo(contexto);

				foreach (OPERADORA o in lista)
				{
					logger.info("Operadora: " + o.DESCRICAO);
					logger.info("Cooperativa: " + o.SUPERINTENDENCIA.NOME);

					processo = pn.obterProcesso("p_processar_aniversario");
					
					jaExecutou = processo.PROCESSAMENTO.Where(p => p.DATA_ATUAL == hoje && p.ID_SUPERINTENDENCIA == o.SUPERINTENDENCIA.ID_SUPERINTENDENCIA && p.ID_OPERADORA == o.ID_OPERADORA).Count() > 0;

					if ("M".Equals(processo.TIPO) || !jaExecutou)
					{
                        idProcessamento = procn.iniciar(processo.ID_PROCESSO, o.SUPERINTENDENCIA.ID_SUPERINTENDENCIA, o.ID_OPERADORA, hoje);

                        logger.info("Processamento dos aniversários.");

						sqlParams = new SqlParameter[]
						{
							new SqlParameter { ParameterName = "@data_atual",				Value = hoje , Direction = System.Data.ParameterDirection.Input},
							new SqlParameter { ParameterName = "@codigo_superintendencia",  Value = o.SUPERINTENDENCIA.CODIGO_SUPERINTENDENCIA, Direction = System.Data.ParameterDirection.Input },
							new SqlParameter { ParameterName = "@id_operadora",				Value = o.ID_OPERADORA, Direction = System.Data.ParameterDirection.Input },
						};

						contexto.Database.ExecuteSqlCommand("execute dbo.p_processar_aniversario @data_atual, @codigo_superintendencia, @id_operadora", sqlParams);

						logger.info("Aniversários processados.");

						procn.terminar(idProcessamento);
					}
					else
					{
						logger.info("O processamento dos aniversários já foi executado anteriormente.");
					}


					//2.- Recalcular parcelas
					processo = pn.obterProcesso("p_recalcular_contrato");

					jaExecutou = processo.PROCESSAMENTO.Where(p => p.DATA_ATUAL == hoje && p.ID_SUPERINTENDENCIA == o.SUPERINTENDENCIA.ID_SUPERINTENDENCIA && p.ID_OPERADORA == o.ID_OPERADORA).Count() > 0;

					if ("M".Equals(processo.TIPO) || ! jaExecutou)
					{
						idProcessamento = procn.iniciar(processo.ID_PROCESSO, o.SUPERINTENDENCIA.ID_SUPERINTENDENCIA, o.ID_OPERADORA, hoje);

						logger.info("Recálculo de contratos.");

						sqlParams = new SqlParameter[]
						{
							new SqlParameter { ParameterName = "@data_atual",				Value = hoje , Direction = System.Data.ParameterDirection.Input},
							new SqlParameter { ParameterName = "@codigo_superintendencia",  Value = o.SUPERINTENDENCIA.CODIGO_SUPERINTENDENCIA, Direction = System.Data.ParameterDirection.Input },
							new SqlParameter { ParameterName = "@id_operadora",				Value = o.ID_OPERADORA, Direction = System.Data.ParameterDirection.Input },
						};

						contexto.Database.ExecuteSqlCommand("execute dbo.p_recalcular_contratos @data_atual, @codigo_superintendencia, @id_operadora", sqlParams);

						logger.info("Contratos recalculados.");

						procn.terminar(idProcessamento);
					}
					else
					{
						logger.info("O recálculo de parcelas já foi executado anteriormente.");
					}
				}

				//COIMPPA
				//3.- Importar o 579
				processo = pn.obterProcesso("p_importar_579");

				jaExecutou = processo.PROCESSAMENTO.Where(p => p.DATA_ATUAL == hoje && p.ID_SUPERINTENDENCIA == 4170 && p.ID_OPERADORA == 2 && p.SITUACAO.Equals("T")).Count() > 0;

				if ("M".Equals(processo.TIPO) || !jaExecutou)
				{
                    idProcessamento = procn.iniciar(processo.ID_PROCESSO, 4170, 2, hoje);

                    logger.info("Início da importação do arquivo de folha do 579.");

					sqlParams = new SqlParameter[]
					{
						new SqlParameter { ParameterName = "@data_atual",				Value = hoje , Direction = System.Data.ParameterDirection.Input},
						new SqlParameter { ParameterName = "@codigo_superintendencia",  Value = 4170, Direction = System.Data.ParameterDirection.Input },
						new SqlParameter { ParameterName = "@id_operadora",				Value = 2, Direction = System.Data.ParameterDirection.Input },
					};
                    try
                    {
                        contexto.Database.ExecuteSqlCommand("execute dbo.p_importar_579 @data_atual, @codigo_superintendencia, @id_operadora", sqlParams);

                        procn.terminar(idProcessamento);

                        logger.info("Fim da importação do arquivo de folha do 579.");

                    }
                    catch (SqlException e)
                    {
                        logger.info("Aconteceu um erro no processamento veja se o arquivo esta na pasta segue a linha de erro \n" + "Erro na procedure " + e.Procedure);
                    }

				}
				else
				{
					logger.info("A importação do arquivo de folha do 579 já foi realizada anteriormente.");
				}



				//4.- Importar o SEAD
				processo = pn.obterProcesso("p_importar_SEAD");

				jaExecutou = processo.PROCESSAMENTO.Where(p => p.DATA_ATUAL == hoje && p.ID_SUPERINTENDENCIA == 4170 && p.ID_OPERADORA == 2 && p.SITUACAO.Equals("T")).Count() > 0;

				if ("M".Equals(processo.TIPO) || !jaExecutou)
				{
					idProcessamento = procn.iniciar(processo.ID_PROCESSO, 4170, 2, hoje);

					logger.info("Início da importação do arquivo de folha da SEAD.");

					sqlParams = new SqlParameter[]
					{
						new SqlParameter { ParameterName = "@data_atual",				Value = hoje , Direction = System.Data.ParameterDirection.Input},
						new SqlParameter { ParameterName = "@codigo_superintendencia",  Value = 4170, Direction = System.Data.ParameterDirection.Input },
						new SqlParameter { ParameterName = "@id_operadora",				Value = 2, Direction = System.Data.ParameterDirection.Input },
					};
                    try
                    {
                        contexto.Database.ExecuteSqlCommand("execute dbo.p_importar_sead_coimppa @data_atual, @codigo_superintendencia, @id_operadora", sqlParams);

                        procn.terminar(idProcessamento);

                        logger.info("Fim da importação do arquivo de folha da SEAD.");

                    }
                    catch (SqlException e)
                    {
                        logger.info("Aconteceu um erro no processamento veja se o arquivo esta na pasta segue a linha de erro \n" + "Erro na procedure " + e.Procedure);
                    }

				}
				else
				{
					logger.info("A importação do arquivo de folha da SEAD já foi realizada anteriormente.");
				}

				//5.- Importar o arquivo de pagamentos do MPPA
				processo = pn.obterProcesso("MPPA");

				jaExecutou = processo.PROCESSAMENTO.Where(p => p.DATA_ATUAL == hoje && p.ID_SUPERINTENDENCIA == 4170 && p.ID_OPERADORA == 2).Count() > 0;
                try
                {
                    if ("M".Equals(processo.TIPO) || !jaExecutou)
                    {
                        idProcessamento = procn.iniciar(processo.ID_PROCESSO, 4170, 2, hoje);

                        logger.info("Carregando o arquivo do MPPA.");

                        MPPA mppa = new MPPA(contexto);
                        mppa.dataAtual = hoje;

                        List<Pagamento> pags = new List<Pagamento>();
                        pags = mppa.carregar();

                        logger.info("O arquivo tinha " + pags.Count + " pagamentos.");
                        if (pags.Count > 0)
                            mppa.baixar(pags);

                        logger.info("Fim do processamento do MPPA.");

                        procn.terminar(idProcessamento);
                    }
                    else
                    {
                        logger.info("A importação do arquivo de folha do MPPA já foi realizada anteriormente.");
                    }
                }
                catch(Exception e)
                {
                    logger.info("Aconteceu um erro no processamento veja se o arquivo esta na pasta segue a linha de erro \n" + e.Message);
                }
				//6.- Importar saldos UNICOOB
				processo = pn.obterProcesso("p_importar_saldo");

				jaExecutou = processo.PROCESSAMENTO.Where(p => p.DATA_ATUAL == hoje && p.ID_SUPERINTENDENCIA == 4170 && p.SITUACAO.Equals("T")).Count() > 0;

				if ("M".Equals(processo.TIPO) || !jaExecutou)
				{
					idProcessamento = procn.iniciar(processo.ID_PROCESSO, 4170,hoje);

					logger.info("Início da importação do arquivo de SALDOS.");

					sqlParams = new SqlParameter[]
					{
						new SqlParameter { ParameterName = "@data_atual",				Value = hoje , Direction = System.Data.ParameterDirection.Input},
						new SqlParameter { ParameterName = "@codigo_superintendencia",  Value = 4170, Direction = System.Data.ParameterDirection.Input },
					};
                    try
                    {


                        contexto.Database.ExecuteSqlCommand("execute dbo.p_importar_saldo @data_atual, @codigo_superintendencia", sqlParams);

                        logger.info("Fim da importação do arquivo de SALDOS.");

                        procn.terminar(idProcessamento);
                    }
                    catch (SqlException e)
                    {
                        logger.info("Aconteceu um erro no processamento veja se o arquivo esta na pasta segue a linha de erro \n" + "Erro na procedure " + e.Procedure);
                    }

                }
				else
				{
					logger.info("A importação do arquivo de saldos UNICOOB já foi realizada anteriormente.");
				}

				//7.- Gerar o DBCC UNICOOB
				processo = pn.obterProcesso("p_exportar_debito_conta");

				jaExecutou = processo.PROCESSAMENTO.Where(p => p.DATA_ATUAL == hoje && p.ID_SUPERINTENDENCIA == 4170 && p.ID_OPERADORA == 2 && p.SITUACAO.Equals("T")).Count() > 0;

				if ("M".Equals(processo.TIPO) || !jaExecutou)
				{
					if (arquivo.existe(998, hoje))
					{
						idProcessamento = procn.iniciar(processo.ID_PROCESSO, 4170, 2,hoje);

						logger.info("Início da exportação do arquivo de DÉBITO CC UNICOOB.");

						logger.info("Verificando que um arquivo de saldos tenha sido importado na data atual.");

						sqlParams = new SqlParameter[]
						{
							new SqlParameter { ParameterName = "@data_atual",				Value = hoje , Direction = System.Data.ParameterDirection.Input},
							new SqlParameter { ParameterName = "@codigo_superintendencia",  Value = 4170, Direction = System.Data.ParameterDirection.Input },
							new SqlParameter { ParameterName = "@id_operadora",				Value = 2, Direction = System.Data.ParameterDirection.Input },
						};
                        try
                        {


                            contexto.Database.ExecuteSqlCommand("execute dbo.p_exportar_debito_conta @data_atual, @codigo_superintendencia, @id_operadora", sqlParams);

                            logger.info("Fim da exportação do arquivo de DÉBITO CC UNICOOB.");

                            procn.terminar(idProcessamento);
                        }
                        catch (SqlException e)
                        {
                            logger.info("Aconteceu um erro no processamento veja se a pasta do arquivo esta no lugar certo a linha de erro \n" + "Erro na procedure " + e.Procedure);
                        }

                    }
					else
					{
						logger.info("O arquivo de saldos ainda não foi importado. O arquivo de DBCC não será exportado.");
					}

				}
				else
				{
					logger.info("A exportação do arquivo de débitos em conta corrente UNICOOB já foi realizada anteriormente.");
				}

				logger.info("Fim do Processamento.");
                System.Threading.Thread.Sleep(50000);
			}
			catch (Exception e)
			{
				logger.error(e.Message);
				    logger.error(e.StackTrace.ToString());

				procn.erro(idProcessamento);
			}
			
		}
	}
}
