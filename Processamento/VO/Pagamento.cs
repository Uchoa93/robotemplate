﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Processamento.VO
{
	class Pagamento
	{
		public string pasta { get; set; }
		public string nomeArquivo { get; set; }
		public string matricula { get; set; }
		public string nome { get; set; }
		public decimal valor { get; set; }
		public string mesNome { get; set; }
		public int mes { get; set; }
		public int ano { get; set; }

		public Pagamento() { }
	}
}
