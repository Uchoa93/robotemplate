namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("REGISTRO")]
    public partial class REGISTRO
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_REGISTRO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ID_ARQUIVO { get; set; }

        [Column("REGISTRO")]
        [Required]
        [StringLength(1000)]
        public string REGISTRO1 { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SEQUENCIAL { get; set; }

        public int? ID_CONTRATO_PARCELA { get; set; }

        [StringLength(200)]
        public string OBSERVACAO { get; set; }

        public decimal SITUACAO { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DATA_ATUALIZACAO { get; set; }

        [StringLength(20)]
        public string USUARIO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string ESTACAO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string OBJETO_ATUALIZACAO { get; set; }

        public virtual ARQUIVO ARQUIVO { get; set; }
    }
}
