namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PAGAMENTO_FORMA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PAGAMENTO_FORMA()
        {
            CONTRATO_PARCELA = new HashSet<CONTRATO_PARCELA>();
            PAGTO_FORMA_PESSOA = new HashSet<PAGTO_FORMA_PESSOA>();
            PAGTO_FORMA_PLANO = new HashSet<PAGTO_FORMA_PLANO>();
        }

        [Key]
        public int ID_PAGAMENTO_FORMA { get; set; }

        public int? ID_PAGAMENTO_TIPO { get; set; }

        public int? ID_PAGTO_FORMA_LAYOUT { get; set; }

        [StringLength(200)]
        public string DESCRICAO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTRATO_PARCELA> CONTRATO_PARCELA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PAGTO_FORMA_PESSOA> PAGTO_FORMA_PESSOA { get; set; }

        public virtual PAGAMENTO_TIPO PAGAMENTO_TIPO { get; set; }

        public virtual PAGTO_FORMA_LAYOUT PAGTO_FORMA_LAYOUT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PAGTO_FORMA_PLANO> PAGTO_FORMA_PLANO { get; set; }
    }
}
