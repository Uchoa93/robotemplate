namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PARAMETRO_VALOR
    {
        [Key]
        public int id_parametro_valor { get; set; }

        public int id_parametro { get; set; }

        public int? codigo_superintendencia { get; set; }

        [Required]
        [StringLength(100)]
        public string valor { get; set; }

        [Required]
        [StringLength(1)]
        public string situacao { get; set; }

        public DateTime data_atualizacao { get; set; }

        public virtual PARAMETRO PARAMETRO { get; set; }
    }
}
