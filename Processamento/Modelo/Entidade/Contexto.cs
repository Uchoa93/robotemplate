﻿namespace Processamento.Modelo.Entidade
{
	using System;
	using System.Data.Entity;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Linq;

	public partial class Contexto : DbContext
	{
		public Contexto()
			: base("name=PD_PRODUTOS")
		{
		}

		public virtual DbSet<ARQUIVO> ARQUIVO { get; set; }
		public virtual DbSet<AUDITORIA> AUDITORIA { get; set; }
		public virtual DbSet<CONTRATO> CONTRATO { get; set; }
		public virtual DbSet<CONTRATO_PARCELA> CONTRATO_PARCELA { get; set; }
		public virtual DbSet<CONTRATO_PARTICIPANTES> CONTRATO_PARTICIPANTES { get; set; }
		public virtual DbSet<DATA_PROCESSAMENTO> DATA_PROCESSAMENTO { get; set; }
		public virtual DbSet<ENDERECO> ENDERECO { get; set; }
		public virtual DbSet<ESTADO> ESTADO { get; set; }
		public virtual DbSet<ESTATUTO_IDOSO> ESTATUTO_IDOSO { get; set; }
		public virtual DbSet<EVENTO> EVENTO { get; set; }
		public virtual DbSet<FERIADO> FERIADO { get; set; }
		public virtual DbSet<INTEGRACAO> INTEGRACAO { get; set; }
		public virtual DbSet<LOG_PROCESSO> LOG_PROCESSO { get; set; }
		public virtual DbSet<MOVIMENTO> MOVIMENTO { get; set; }
		public virtual DbSet<MUNICIPIO> MUNICIPIO { get; set; }
		public virtual DbSet<OPERADORA> OPERADORA { get; set; }
		public virtual DbSet<PAGAMENTO_FORMA> PAGAMENTO_FORMA { get; set; }
		public virtual DbSet<PAGAMENTO_TIPO> PAGAMENTO_TIPO { get; set; }
		public virtual DbSet<PAGTO_FORMA_LAYOUT> PAGTO_FORMA_LAYOUT { get; set; }
		public virtual DbSet<PAGTO_FORMA_PESSOA> PAGTO_FORMA_PESSOA { get; set; }
		public virtual DbSet<PAGTO_FORMA_PLANO> PAGTO_FORMA_PLANO { get; set; }
		public virtual DbSet<PARAMETRO> PARAMETRO { get; set; }
		public virtual DbSet<PARAMETRO_VALOR> PARAMETRO_VALOR { get; set; }
		public virtual DbSet<PARCELA_PARTICIPANTE> PARCELA_PARTICIPANTE { get; set; }
		public virtual DbSet<PESSOA> PESSOA { get; set; }
		public virtual DbSet<PESSOA_ENDERECO_ELETRONICO> PESSOA_ENDERECO_ELETRONICO { get; set; }
		public virtual DbSet<PESSOA_FISICA> PESSOA_FISICA { get; set; }
		public virtual DbSet<PESSOA_JURIDICA> PESSOA_JURIDICA { get; set; }
		public virtual DbSet<PESSOA_OCUPACAO> PESSOA_OCUPACAO { get; set; }
		public virtual DbSet<PESSOA_RELACIONAMENTO> PESSOA_RELACIONAMENTO { get; set; }
		public virtual DbSet<PESSOA_TELEFONE> PESSOA_TELEFONE { get; set; }
		public virtual DbSet<PLANO> PLANO { get; set; }
		public virtual DbSet<PLANO_DIAS> PLANO_DIAS { get; set; }
		public virtual DbSet<PLANO_VALOR> PLANO_VALOR { get; set; }
		public virtual DbSet<PROCESSAMENTO> PROCESSAMENTO { get; set; }
		public virtual DbSet<PROCESSO> PROCESSO { get; set; }
		public virtual DbSet<REGISTRO> REGISTRO { get; set; }
		public virtual DbSet<SALDO> SALDO { get; set; }
		public virtual DbSet<SUPERINTENDENCIA> SUPERINTENDENCIA { get; set; }
		public virtual DbSet<TABELA_VALORES> TABELA_VALORES { get; set; }
		public virtual DbSet<UNIDADE> UNIDADE { get; set; }
		public virtual DbSet<VALOR_FAIXA_ETARIA> VALOR_FAIXA_ETARIA { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Entity<ARQUIVO>()
				.Property(e => e.ID_ARQUIVO)
				.HasPrecision(10, 0);

			modelBuilder.Entity<ARQUIVO>()
				.Property(e => e.ID_INTEGRACAO)
				.HasPrecision(10, 0);

			modelBuilder.Entity<ARQUIVO>()
				.Property(e => e.CODIGO_INTEGRACAO)
				.HasPrecision(3, 0);

			modelBuilder.Entity<ARQUIVO>()
				.Property(e => e.PASTA)
				.IsUnicode(false);

			modelBuilder.Entity<ARQUIVO>()
				.Property(e => e.NOME)
				.IsUnicode(false);

			modelBuilder.Entity<ARQUIVO>()
				.Property(e => e.QUANT_REGISTROS)
				.HasPrecision(6, 0);

			modelBuilder.Entity<ARQUIVO>()
				.Property(e => e.SITUACAO)
				.HasPrecision(2, 0);

			modelBuilder.Entity<ARQUIVO>()
				.Property(e => e.USUARIO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<ARQUIVO>()
				.Property(e => e.ESTACAO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<ARQUIVO>()
				.Property(e => e.OBJETO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<ARQUIVO>()
				.HasMany(e => e.REGISTRO)
				.WithRequired(e => e.ARQUIVO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<AUDITORIA>()
				.Property(e => e.APP)
				.IsUnicode(false);

			modelBuilder.Entity<AUDITORIA>()
				.Property(e => e.DS_USUARIO_BD)
				.IsUnicode(false);

			modelBuilder.Entity<AUDITORIA>()
				.Property(e => e.DS_MAQUINA_BD)
				.IsUnicode(false);

			modelBuilder.Entity<AUDITORIA>()
				.Property(e => e.TABELA)
				.IsUnicode(false);

			modelBuilder.Entity<AUDITORIA>()
				.Property(e => e.OPERACAO)
				.IsUnicode(false);

			modelBuilder.Entity<AUDITORIA>()
				.Property(e => e.DS_USUARIO)
				.IsUnicode(false);

			modelBuilder.Entity<AUDITORIA>()
				.Property(e => e.IP_MAQUINA)
				.IsUnicode(false);

			modelBuilder.Entity<AUDITORIA>()
				.Property(e => e.DS_MAQUINA)
				.IsUnicode(false);

			modelBuilder.Entity<CONTRATO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<CONTRATO>()
				.Property(e => e.VALOR_GLOBAL_ADESAO)
				.HasPrecision(12, 2);

			modelBuilder.Entity<CONTRATO>()
				.Property(e => e.VALOR_GLOBAL_MANUTENCAO)
				.HasPrecision(12, 2);

			modelBuilder.Entity<CONTRATO>()
				.Property(e => e.OBSERVACAO)
				.IsUnicode(false);

			modelBuilder.Entity<CONTRATO_PARCELA>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<CONTRATO_PARCELA>()
				.Property(e => e.VALOR_PARCELA)
				.HasPrecision(12, 2);

			modelBuilder.Entity<CONTRATO_PARCELA>()
				.Property(e => e.VALOR_PAGO)
				.HasPrecision(12, 2);

			modelBuilder.Entity<CONTRATO_PARCELA>()
				.Property(e => e.VALOR_PAGO_MULTA)
				.HasPrecision(12, 2);

			modelBuilder.Entity<CONTRATO_PARCELA>()
				.Property(e => e.VALOR_PAGO_JUROS)
				.HasPrecision(18, 0);

			modelBuilder.Entity<CONTRATO_PARTICIPANTES>()
				.Property(e => e.VALOR_ADESAO)
				.HasPrecision(12, 2);

			modelBuilder.Entity<CONTRATO_PARTICIPANTES>()
				.Property(e => e.VALOR_MANUTENCAO)
				.HasPrecision(12, 2);

			modelBuilder.Entity<CONTRATO_PARTICIPANTES>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<CONTRATO_PARTICIPANTES>()
				.Property(e => e.estatuto_idoso)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<DATA_PROCESSAMENTO>()
				.Property(e => e.ID_DATA_PROCESSAMENTO)
				.HasPrecision(10, 0);

			modelBuilder.Entity<DATA_PROCESSAMENTO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<DATA_PROCESSAMENTO>()
				.Property(e => e.USUARIO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<DATA_PROCESSAMENTO>()
				.Property(e => e.ESTACAO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<DATA_PROCESSAMENTO>()
				.Property(e => e.OBJETO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<ENDERECO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ESTADO>()
				.Property(e => e.id_estado)
				.HasPrecision(10, 0);

			modelBuilder.Entity<ESTADO>()
				.Property(e => e.nome)
				.IsUnicode(false);

			modelBuilder.Entity<ESTADO>()
				.Property(e => e.sigla)
				.IsUnicode(false);

			modelBuilder.Entity<ESTADO>()
				.Property(e => e.situacao)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<ESTADO>()
				.Property(e => e.atualizacao_estacao)
				.IsUnicode(false);

			modelBuilder.Entity<ESTADO>()
				.Property(e => e.atualizacao_objeto)
				.IsUnicode(false);

			modelBuilder.Entity<ESTADO>()
				.Property(e => e.atualizacao_usuario)
				.IsUnicode(false);

			modelBuilder.Entity<ESTADO>()
				.HasMany(e => e.MUNICIPIO)
				.WithRequired(e => e.ESTADO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<ESTATUTO_IDOSO>()
				.Property(e => e.nome_objeto)
				.IsUnicode(false);

			modelBuilder.Entity<EVENTO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<FERIADO>()
				.Property(e => e.id_feriado)
				.HasPrecision(10, 0);

			modelBuilder.Entity<FERIADO>()
				.Property(e => e.ano)
				.HasPrecision(4, 0);

			modelBuilder.Entity<FERIADO>()
				.Property(e => e.mes)
				.HasPrecision(2, 0);

			modelBuilder.Entity<FERIADO>()
				.Property(e => e.dia)
				.HasPrecision(2, 0);

			modelBuilder.Entity<FERIADO>()
				.Property(e => e.tipo_feriado)
				.HasPrecision(1, 0);

			modelBuilder.Entity<FERIADO>()
				.Property(e => e.id_estado)
				.HasPrecision(10, 0);

			modelBuilder.Entity<FERIADO>()
				.Property(e => e.id_municipio)
				.HasPrecision(10, 0);

			modelBuilder.Entity<FERIADO>()
				.Property(e => e.descrição)
				.IsUnicode(false);

			modelBuilder.Entity<FERIADO>()
				.Property(e => e.situacao)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<FERIADO>()
				.Property(e => e.atualizacao_estacao)
				.IsUnicode(false);

			modelBuilder.Entity<FERIADO>()
				.Property(e => e.atualizacao_objeto)
				.IsUnicode(false);

			modelBuilder.Entity<FERIADO>()
				.Property(e => e.atualizacao_usuario)
				.IsUnicode(false);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.ID_INTEGRACAO)
				.HasPrecision(10, 0);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.CODIGO_INTEGRACAO)
				.HasPrecision(6, 0);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.PASTA)
				.IsUnicode(false);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.PASTA_PROCESSADOS)
				.IsUnicode(false);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.TAMANHO_REGISTRO)
				.HasPrecision(5, 0);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.PREFIXO)
				.IsUnicode(false);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.EXTENSAO)
				.IsUnicode(false);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.TIPO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.USUARIO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.ESTACAO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<INTEGRACAO>()
				.Property(e => e.OBJETO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<INTEGRACAO>()
				.HasMany(e => e.ARQUIVO)
				.WithRequired(e => e.INTEGRACAO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<LOG_PROCESSO>()
				.Property(e => e.id_log_processo)
				.HasPrecision(15, 0);

			modelBuilder.Entity<LOG_PROCESSO>()
				.Property(e => e.nome_rotina)
				.IsUnicode(false);

			modelBuilder.Entity<LOG_PROCESSO>()
				.Property(e => e.mensagem)
				.IsUnicode(false);

			modelBuilder.Entity<LOG_PROCESSO>()
				.Property(e => e.tipo)
				.IsUnicode(false);

			modelBuilder.Entity<MOVIMENTO>()
				.Property(e => e.VALOR)
				.HasPrecision(12, 2);

			modelBuilder.Entity<MOVIMENTO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<MUNICIPIO>()
				.Property(e => e.id_municipio)
				.HasPrecision(10, 0);

			modelBuilder.Entity<MUNICIPIO>()
				.Property(e => e.id_estado)
				.HasPrecision(10, 0);

			modelBuilder.Entity<MUNICIPIO>()
				.Property(e => e.nome)
				.IsUnicode(false);

			modelBuilder.Entity<MUNICIPIO>()
				.Property(e => e.situacao)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<MUNICIPIO>()
				.Property(e => e.atualizacao_estacao)
				.IsUnicode(false);

			modelBuilder.Entity<MUNICIPIO>()
				.Property(e => e.atualizacao_objeto)
				.IsUnicode(false);

			modelBuilder.Entity<MUNICIPIO>()
				.Property(e => e.atualizacao_usuario)
				.IsUnicode(false);

			modelBuilder.Entity<OPERADORA>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PAGAMENTO_FORMA>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PAGAMENTO_TIPO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PAGTO_FORMA_LAYOUT>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PAGTO_FORMA_PESSOA>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PARAMETRO>()
				.Property(e => e.descricao)
				.IsUnicode(false);

			modelBuilder.Entity<PARAMETRO>()
				.Property(e => e.chave)
				.IsUnicode(false);

			modelBuilder.Entity<PARAMETRO>()
				.Property(e => e.situacao)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PARAMETRO>()
				.HasMany(e => e.PARAMETRO_VALOR)
				.WithRequired(e => e.PARAMETRO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<PARAMETRO_VALOR>()
				.Property(e => e.valor)
				.IsUnicode(false);

			modelBuilder.Entity<PARAMETRO_VALOR>()
				.Property(e => e.situacao)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PARCELA_PARTICIPANTE>()
				.Property(e => e.ID_PARCELA_PARTICIPANTE)
				.HasPrecision(10, 0);

			modelBuilder.Entity<PARCELA_PARTICIPANTE>()
				.Property(e => e.VALOR_PARCELA_INDIVIDUAL)
				.HasPrecision(10, 2);

			modelBuilder.Entity<PARCELA_PARTICIPANTE>()
				.Property(e => e.STATUS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PARCELA_PARTICIPANTE>()
				.Property(e => e.USUARIO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<PARCELA_PARTICIPANTE>()
				.Property(e => e.ESTACAO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<PARCELA_PARTICIPANTE>()
				.Property(e => e.OBJETO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<PESSOA>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PESSOA>()
				.HasMany(e => e.CONTRATO)
				.WithOptional(e => e.PESSOA)
				.HasForeignKey(e => e.ID_PESSOA);

			modelBuilder.Entity<PESSOA>()
				.HasMany(e => e.CONTRATO1)
				.WithOptional(e => e.PESSOA1)
				.HasForeignKey(e => e.ID_PESSOA_FINANCEIRA);

			modelBuilder.Entity<PESSOA>()
				.HasMany(e => e.PESSOA_FISICA)
				.WithRequired(e => e.PESSOA)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<PESSOA>()
				.HasMany(e => e.PESSOA_JURIDICA)
				.WithRequired(e => e.PESSOA)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<PESSOA>()
				.HasMany(e => e.PESSOA_OCUPACAO)
				.WithOptional(e => e.PESSOA)
				.HasForeignKey(e => e.ID_PESSOA);

			modelBuilder.Entity<PESSOA>()
				.HasMany(e => e.PESSOA_OCUPACAO1)
				.WithOptional(e => e.PESSOA1)
				.HasForeignKey(e => e.ID_PESSOA_EMPREGADOR);

			modelBuilder.Entity<PESSOA>()
				.HasMany(e => e.PESSOA_RELACIONAMENTO)
				.WithOptional(e => e.PESSOA)
				.HasForeignKey(e => e.ID_PESSOA);

			modelBuilder.Entity<PESSOA>()
				.HasMany(e => e.PESSOA_RELACIONAMENTO1)
				.WithOptional(e => e.PESSOA1)
				.HasForeignKey(e => e.ID_PESSOA_RELACIONADA);

			modelBuilder.Entity<PESSOA_ENDERECO_ELETRONICO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PESSOA_OCUPACAO>()
				.Property(e => e.VALOR_RENDA_MENSAL)
				.HasPrecision(12, 2);

			modelBuilder.Entity<PESSOA_OCUPACAO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PESSOA_RELACIONAMENTO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PESSOA_TELEFONE>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PLANO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PLANO>()
				.Property(e => e.SEGMENTO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PLANO>()
				.Property(e => e.MULTA_PERCENTUAL)
				.HasPrecision(5, 2);

			modelBuilder.Entity<PLANO>()
				.Property(e => e.MORA_PERCENTUAL)
				.HasPrecision(5, 2);

			modelBuilder.Entity<PLANO>()
				.Property(e => e.PERCENTUAL_OPERADORA)
				.HasPrecision(5, 2);

			modelBuilder.Entity<PLANO>()
				.HasMany(e => e.VALOR_FAIXA_ETARIA)
				.WithRequired(e => e.PLANO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<PLANO>()
				.HasMany(e => e.PLANO_VALOR)
				.WithRequired(e => e.PLANO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<PLANO_DIAS>()
				.Property(e => e.ACEITA_VENCTO)
				.IsFixedLength();

			modelBuilder.Entity<PLANO_VALOR>()
				.Property(e => e.VALOR_ADESAO)
				.HasPrecision(12, 2);

			modelBuilder.Entity<PLANO_VALOR>()
				.Property(e => e.VALOR_MANUTENCAO)
				.HasPrecision(12, 2);

			modelBuilder.Entity<PLANO_VALOR>()
				.Property(e => e.VALOR_LIVRE)
				.IsFixedLength();

			modelBuilder.Entity<PLANO_VALOR>()
				.Property(e => e.VALOR_LIVRE_MINIMO)
				.HasPrecision(12, 2);

			modelBuilder.Entity<PLANO_VALOR>()
				.Property(e => e.VALOR_LIVRE_MAXIMO)
				.HasPrecision(12, 2);

			modelBuilder.Entity<PLANO_VALOR>()
				.Property(e => e.SITUACAO)
				.IsFixedLength();

			modelBuilder.Entity<PROCESSAMENTO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PROCESSAMENTO>()
				.Property(e => e.USUARIO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<PROCESSAMENTO>()
				.Property(e => e.OBJETO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<PROCESSO>()
				.Property(e => e.DESCRICAO)
				.IsUnicode(false);

			modelBuilder.Entity<PROCESSO>()
				.Property(e => e.NOME_ROTINA)
				.IsUnicode(false);

			modelBuilder.Entity<PROCESSO>()
				.Property(e => e.TIPO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PROCESSO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<PROCESSO>()
				.Property(e => e.USUARIO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<PROCESSO>()
				.Property(e => e.OBJETO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<PROCESSO>()
				.HasMany(e => e.PROCESSAMENTO)
				.WithRequired(e => e.PROCESSO)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<REGISTRO>()
				.Property(e => e.ID_REGISTRO)
				.HasPrecision(15, 0);

			modelBuilder.Entity<REGISTRO>()
				.Property(e => e.ID_ARQUIVO)
				.HasPrecision(10, 0);

			modelBuilder.Entity<REGISTRO>()
				.Property(e => e.REGISTRO1)
				.IsUnicode(false);

			modelBuilder.Entity<REGISTRO>()
				.Property(e => e.SEQUENCIAL)
				.HasPrecision(6, 0);

			modelBuilder.Entity<REGISTRO>()
				.Property(e => e.OBSERVACAO)
				.IsUnicode(false);

			modelBuilder.Entity<REGISTRO>()
				.Property(e => e.SITUACAO)
				.HasPrecision(2, 0);

			modelBuilder.Entity<REGISTRO>()
				.Property(e => e.USUARIO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<REGISTRO>()
				.Property(e => e.ESTACAO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<REGISTRO>()
				.Property(e => e.OBJETO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<SALDO>()
				.Property(e => e.CODIGO_AGENCIA)
				.HasPrecision(5, 0);

			modelBuilder.Entity<SALDO>()
				.Property(e => e.NUMERO_CONTA)
				.HasPrecision(12, 0);

			modelBuilder.Entity<SALDO>()
				.Property(e => e.VALOR_CONTA)
				.HasPrecision(15, 2);

			modelBuilder.Entity<SALDO>()
				.Property(e => e.VALOR_CHEQUE_ESPECIAL)
				.HasPrecision(15, 2);

			modelBuilder.Entity<SALDO>()
				.Property(e => e.VALOR_TOTAL)
				.HasPrecision(15, 2);

			modelBuilder.Entity<SALDO>()
				.Property(e => e.SALDO_DISPONIVEL)
				.HasPrecision(15, 2);

			modelBuilder.Entity<SALDO>()
				.Property(e => e.ID_REGISTRO)
				.HasPrecision(15, 0);

			modelBuilder.Entity<SALDO>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<SALDO>()
				.Property(e => e.USUARIO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<SALDO>()
				.Property(e => e.ESTACAO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<SALDO>()
				.Property(e => e.OBJETO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<SUPERINTENDENCIA>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<TABELA_VALORES>()
				.Property(e => e.ID_TABELA_VALORES)
				.HasPrecision(10, 0);

			modelBuilder.Entity<TABELA_VALORES>()
				.Property(e => e.ID_OPERADORA)
				.HasPrecision(6, 0);

			modelBuilder.Entity<TABELA_VALORES>()
				.Property(e => e.STATUS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<TABELA_VALORES>()
				.Property(e => e.USUARIO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<TABELA_VALORES>()
				.Property(e => e.ESTACAO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<TABELA_VALORES>()
				.Property(e => e.OBJETO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<TABELA_VALORES>()
				.HasMany(e => e.VALOR_FAIXA_ETARIA)
				.WithRequired(e => e.TABELA_VALORES)
				.WillCascadeOnDelete(false);

			modelBuilder.Entity<UNIDADE>()
				.Property(e => e.SITUACAO)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<VALOR_FAIXA_ETARIA>()
				.Property(e => e.ID_VALOR_FAIXA_ETARIA)
				.HasPrecision(10, 0);

			modelBuilder.Entity<VALOR_FAIXA_ETARIA>()
				.Property(e => e.VALOR_OPERADORA)
				.HasPrecision(10, 2);

			modelBuilder.Entity<VALOR_FAIXA_ETARIA>()
				.Property(e => e.VALOR_SUPERINTENDENCIA)
				.HasPrecision(10, 2);

			modelBuilder.Entity<VALOR_FAIXA_ETARIA>()
				.Property(e => e.ID_TABELA_VALORES)
				.HasPrecision(10, 0);

			modelBuilder.Entity<VALOR_FAIXA_ETARIA>()
				.Property(e => e.STATUS)
				.IsFixedLength()
				.IsUnicode(false);

			modelBuilder.Entity<VALOR_FAIXA_ETARIA>()
				.Property(e => e.USUARIO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<VALOR_FAIXA_ETARIA>()
				.Property(e => e.ESTACAO_ATUALIZACAO)
				.IsUnicode(false);

			modelBuilder.Entity<VALOR_FAIXA_ETARIA>()
				.Property(e => e.OBJETO_ATUALIZACAO)
				.IsUnicode(false);
		}
	}
}
