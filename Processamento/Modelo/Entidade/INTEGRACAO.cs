namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("INTEGRACAO")]
    public partial class INTEGRACAO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INTEGRACAO()
        {
            ARQUIVO = new HashSet<ARQUIVO>();
        }

        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_INTEGRACAO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CODIGO_INTEGRACAO { get; set; }

        [StringLength(150)]
        public string DESCRICAO { get; set; }

        [StringLength(100)]
        public string PASTA { get; set; }

        [StringLength(100)]
        public string PASTA_PROCESSADOS { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? TAMANHO_REGISTRO { get; set; }

        [Required]
        [StringLength(40)]
        public string PREFIXO { get; set; }

        [Required]
        [StringLength(20)]
        public string EXTENSAO { get; set; }

        [Required]
        [StringLength(1)]
        public string TIPO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DATA_ATUALIZACAO { get; set; }

        [StringLength(20)]
        public string USUARIO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string ESTACAO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string OBJETO_ATUALIZACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ARQUIVO> ARQUIVO { get; set; }
    }
}
