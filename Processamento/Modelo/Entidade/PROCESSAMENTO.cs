namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PROCESSAMENTO")]
    public partial class PROCESSAMENTO
    {
        [Key]
        public int ID_PROCESSAMENTO { get; set; }

        public int ID_PROCESSO { get; set; }

        [Column(TypeName = "date")]
        public DateTime DATA_ATUAL { get; set; }

        public int? ID_SUPERINTENDENCIA { get; set; }

        public int? ID_OPERADORA { get; set; }

        public DateTime? DATA_INICIO { get; set; }

        public DateTime? DATA_FIM { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        public DateTime DATA_ATUALIZACAO { get; set; }

        [StringLength(50)]
        public string USUARIO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string OBJETO_ATUALIZACAO { get; set; }

        public virtual PROCESSO PROCESSO { get; set; }
    }
}
