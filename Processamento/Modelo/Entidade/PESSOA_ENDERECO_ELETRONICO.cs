namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PESSOA_ENDERECO_ELETRONICO
    {
        [Key]
        public int ID_PESSOA_END_ELETRONICO { get; set; }

        public int? ID_PESSOA { get; set; }

        [StringLength(200)]
        public string ENDERECO_ELETRONICO { get; set; }

        [StringLength(1)]
        public string CODIGO_ENDERECO_ELETRONICO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        public virtual PESSOA PESSOA { get; set; }
    }
}
