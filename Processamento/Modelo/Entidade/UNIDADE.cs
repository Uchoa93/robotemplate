namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UNIDADE")]
    public partial class UNIDADE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UNIDADE()
        {
            CONTRATO = new HashSet<CONTRATO>();
            MOVIMENTO = new HashSet<MOVIMENTO>();
            PESSOA = new HashSet<PESSOA>();
        }

        [Key]
        public int ID_UNIDADE { get; set; }

        public int? ID_SUPERINTENDENCIA { get; set; }

        public int? ID_ENDERECO { get; set; }

        public int? CODIGO { get; set; }

        [Required]
        [StringLength(50)]
        public string NOME { get; set; }

        public int? CODIGO_DV { get; set; }

        [StringLength(1)]
        public string TIPO { get; set; }

        [StringLength(14)]
        public string CNPJ { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DATA_INI_VAL { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DATA_FIM_VAL { get; set; }

        public int? ID_UNIDADE_PAI { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTRATO> CONTRATO { get; set; }

        public virtual ENDERECO ENDERECO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MOVIMENTO> MOVIMENTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PESSOA> PESSOA { get; set; }

        public virtual SUPERINTENDENCIA SUPERINTENDENCIA { get; set; }
    }
}
