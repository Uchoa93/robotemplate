namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PESSOA")]
    public partial class PESSOA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PESSOA()
        {
            CONTRATO = new HashSet<CONTRATO>();
            CONTRATO1 = new HashSet<CONTRATO>();
            CONTRATO_PARTICIPANTES = new HashSet<CONTRATO_PARTICIPANTES>();
            PESSOA_ENDERECO_ELETRONICO = new HashSet<PESSOA_ENDERECO_ELETRONICO>();
            PESSOA_FISICA = new HashSet<PESSOA_FISICA>();
            PESSOA_JURIDICA = new HashSet<PESSOA_JURIDICA>();
            PESSOA_OCUPACAO = new HashSet<PESSOA_OCUPACAO>();
            PESSOA_OCUPACAO1 = new HashSet<PESSOA_OCUPACAO>();
            PESSOA_RELACIONAMENTO = new HashSet<PESSOA_RELACIONAMENTO>();
            PESSOA_RELACIONAMENTO1 = new HashSet<PESSOA_RELACIONAMENTO>();
            PESSOA_TELEFONE = new HashSet<PESSOA_TELEFONE>();
        }

        [Key]
        public int ID_PESSOA { get; set; }

        public int? ID_UNIDADE { get; set; }

        public int? ID_ENDERECO { get; set; }

        [StringLength(14)]
        public string CPF_CNPJ { get; set; }

        [StringLength(200)]
        public string NOME { get; set; }

        [StringLength(2)]
        public string TIPO { get; set; }

        [StringLength(50)]
        public string CD_PESSOA_LEGADO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTRATO> CONTRATO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTRATO> CONTRATO1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTRATO_PARTICIPANTES> CONTRATO_PARTICIPANTES { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PESSOA_ENDERECO_ELETRONICO> PESSOA_ENDERECO_ELETRONICO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PESSOA_FISICA> PESSOA_FISICA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PESSOA_JURIDICA> PESSOA_JURIDICA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PESSOA_OCUPACAO> PESSOA_OCUPACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PESSOA_OCUPACAO> PESSOA_OCUPACAO1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PESSOA_RELACIONAMENTO> PESSOA_RELACIONAMENTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PESSOA_RELACIONAMENTO> PESSOA_RELACIONAMENTO1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PESSOA_TELEFONE> PESSOA_TELEFONE { get; set; }

        public virtual UNIDADE UNIDADE { get; set; }
    }
}
