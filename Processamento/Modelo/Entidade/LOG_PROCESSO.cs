namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LOG_PROCESSO
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal id_log_processo { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime data_log { get; set; }

        [Required]
        [StringLength(100)]
        public string nome_rotina { get; set; }

        [StringLength(4000)]
        public string mensagem { get; set; }

        [Column(TypeName = "date")]
        public DateTime? data_atual { get; set; }

        [StringLength(10)]
        public string tipo { get; set; }
    }
}
