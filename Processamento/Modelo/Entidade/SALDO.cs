namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SALDO")]
    public partial class SALDO
    {
        [Key]
        public int ID_SALDO { get; set; }

        [Column(TypeName = "date")]
        public DateTime DATA_SALDO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CODIGO_AGENCIA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal NUMERO_CONTA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal VALOR_CONTA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal VALOR_CHEQUE_ESPECIAL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal VALOR_TOTAL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal SALDO_DISPONIVEL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ID_REGISTRO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DATA_ATUALIZACAO { get; set; }

        [StringLength(20)]
        public string USUARIO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string ESTACAO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string OBJETO_ATUALIZACAO { get; set; }
    }
}
