namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CONTRATO")]
    public partial class CONTRATO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CONTRATO()
        {
            CONTRATO_PARCELA = new HashSet<CONTRATO_PARCELA>();
            CONTRATO_PARTICIPANTES = new HashSet<CONTRATO_PARTICIPANTES>();
            MOVIMENTO = new HashSet<MOVIMENTO>();
        }

        [Key]
        public int ID_CONTRATO { get; set; }

        public int? ID_PESSOA { get; set; }

        public int? ID_PLANO { get; set; }

        public int? DATA_VENCIMENTO { get; set; }

        public int? ID_PESSOA_FINANCEIRA { get; set; }

        public int? ID_UNIDADE { get; set; }

        public int? ID_USUARIO_CONTRATANTE { get; set; }

        public int? ID_PAGTO_FORMA_PESSOA { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DATA_INICIO { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DATA_FIM { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_GLOBAL_ADESAO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_GLOBAL_MANUTENCAO { get; set; }

        public int? NUMERO_VERSAO { get; set; }

        [StringLength(50)]
        public string IDENTIFICACAO_EXTERNA { get; set; }

        [StringLength(500)]
        public string OBSERVACAO { get; set; }

        public virtual PAGTO_FORMA_PESSOA PAGTO_FORMA_PESSOA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTRATO_PARCELA> CONTRATO_PARCELA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTRATO_PARTICIPANTES> CONTRATO_PARTICIPANTES { get; set; }

        public virtual PESSOA PESSOA { get; set; }

        public virtual PESSOA PESSOA1 { get; set; }

        public virtual PLANO PLANO { get; set; }

        public virtual UNIDADE UNIDADE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MOVIMENTO> MOVIMENTO { get; set; }
    }
}
