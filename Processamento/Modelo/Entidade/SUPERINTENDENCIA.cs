namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SUPERINTENDENCIA")]
    public partial class SUPERINTENDENCIA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SUPERINTENDENCIA()
        {
            OPERADORA = new HashSet<OPERADORA>();
            UNIDADE = new HashSet<UNIDADE>();
        }

        [Key]
        public int ID_SUPERINTENDENCIA { get; set; }

        [StringLength(20)]
        public string CODIGO_SUPERINTENDENCIA { get; set; }

        [Required]
        [StringLength(50)]
        public string NOME { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DATA_INI_VAL { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DATA_FIM_VAL { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [StringLength(14)]
        public string CNPJ { get; set; }

        [StringLength(50)]
        public string NOME_RESPONSAVEL { get; set; }

        public int? ID_ENDERECO { get; set; }

        [StringLength(11)]
        public string FONE_CONTATO { get; set; }

        [StringLength(50)]
        public string EMAIL_CONTATO { get; set; }

        public virtual ENDERECO ENDERECO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OPERADORA> OPERADORA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UNIDADE> UNIDADE { get; set; }
    }
}
