namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PESSOA_OCUPACAO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID_PESSOA_OCUPACAO { get; set; }

        public int? ID_PESSOA { get; set; }

        public int? ID_PESSOA_EMPREGADOR { get; set; }

        [StringLength(50)]
        public string CARGO { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? INICIO_RELACIONAMENTO { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? FIM_RELACIONAMENTO { get; set; }

        public int? FL_RENDA_PRINCIPAL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_RENDA_MENSAL { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        public virtual PESSOA PESSOA { get; set; }

        public virtual PESSOA PESSOA1 { get; set; }
    }
}
