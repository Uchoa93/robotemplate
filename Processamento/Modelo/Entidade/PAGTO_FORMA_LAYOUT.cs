namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PAGTO_FORMA_LAYOUT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PAGTO_FORMA_LAYOUT()
        {
            PAGAMENTO_FORMA = new HashSet<PAGAMENTO_FORMA>();
        }

        [Key]
        public int ID_PAGTO_FORMA_LAYOUT { get; set; }

        public int? ID_PAGAMENTO_TIPO { get; set; }

        [StringLength(200)]
        public string DESCRICAO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PAGAMENTO_FORMA> PAGAMENTO_FORMA { get; set; }

        public virtual PAGAMENTO_TIPO PAGAMENTO_TIPO { get; set; }
    }
}
