namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OPERADORA")]
    public partial class OPERADORA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OPERADORA()
        {
            PLANO = new HashSet<PLANO>();
        }

        [Key]
        public int ID_OPERADORA { get; set; }

        public int? ID_SUPERINTENDENCIA { get; set; }

        [StringLength(200)]
        public string DESCRICAO { get; set; }

        [StringLength(50)]
        public string NOME_CONTATO { get; set; }

        [StringLength(11)]
        public string FONE_CONTATO { get; set; }

        [StringLength(50)]
        public string EMAIL_CONTATO { get; set; }

        public int? BANCO { get; set; }

        public int? AGENCIA { get; set; }

        [StringLength(10)]
        public string CONTA_CORRENTE { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DATA_INI_VAL { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DATA_FIM_VAL { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [StringLength(14)]
        public string CNPJ { get; set; }

        public virtual SUPERINTENDENCIA SUPERINTENDENCIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLANO> PLANO { get; set; }
    }
}
