namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EVENTO")]
    public partial class EVENTO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EVENTO()
        {
            MOVIMENTO = new HashSet<MOVIMENTO>();
        }

        [Key]
        public int ID_EVENTO { get; set; }

        public int CODIGO_EVENTO { get; set; }

        [StringLength(100)]
        public string DESCRICAO { get; set; }

        [StringLength(30)]
        public string DESC_RESUMIDA { get; set; }

        [StringLength(1)]
        public string NATUREZA { get; set; }

        public int? ID_EVENTO_ESTORNO { get; set; }

        [StringLength(1)]
        public string EXIBE_EXTRATO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MOVIMENTO> MOVIMENTO { get; set; }
    }
}
