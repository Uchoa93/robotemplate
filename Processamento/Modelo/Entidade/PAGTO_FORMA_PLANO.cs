namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PAGTO_FORMA_PLANO
    {
        [Key]
        public int ID_PAGTO_FORMA_PLANO { get; set; }

        public int? ID_PLANO { get; set; }

        public int? ID_PAGAMENTO_FORMA { get; set; }

        public virtual PAGAMENTO_FORMA PAGAMENTO_FORMA { get; set; }

        public virtual PLANO PLANO { get; set; }
    }
}
