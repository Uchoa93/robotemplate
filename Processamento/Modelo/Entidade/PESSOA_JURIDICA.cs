namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PESSOA_JURIDICA
    {
        [Key]
        public int ID_PESSOA_JURIDICA { get; set; }

        public int ID_PESSOA { get; set; }

        [StringLength(200)]
        public string NM_FANTASIA { get; set; }

        [StringLength(50)]
        public string NR_REGISTRO { get; set; }

        [StringLength(200)]
        public string DS_ATIVIDADE { get; set; }

        [StringLength(50)]
        public string DS_TIPO_EMPRESA { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DT_CADASTRAMENTO { get; set; }

        [StringLength(10)]
        public string CD_ATIVIDADE_ECONOMICA { get; set; }

        [StringLength(200)]
        public string NM_ATIVIDADE_ECONOMICA { get; set; }

        public virtual PESSOA PESSOA { get; set; }
    }
}
