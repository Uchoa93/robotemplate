namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ENDERECO")]
    public partial class ENDERECO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ENDERECO()
        {
            SUPERINTENDENCIA = new HashSet<SUPERINTENDENCIA>();
            UNIDADE = new HashSet<UNIDADE>();
        }

        [Key]
        public int ID_ENDERECO { get; set; }

        [StringLength(2)]
        public string UF { get; set; }

        [StringLength(100)]
        public string CIDADE { get; set; }

        [StringLength(100)]
        public string BAIRRO { get; set; }

        public int? CD_TIPO_ENDERECO { get; set; }

        [StringLength(100)]
        public string NM_LOGRADOURO { get; set; }

        [StringLength(10)]
        public string NR_LOGRADOURO { get; set; }

        [StringLength(255)]
        public string NM_COMPLEMENTO { get; set; }

        [StringLength(8)]
        public string CEP { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SUPERINTENDENCIA> SUPERINTENDENCIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UNIDADE> UNIDADE { get; set; }
    }
}
