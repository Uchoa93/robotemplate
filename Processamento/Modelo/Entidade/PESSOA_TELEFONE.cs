namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PESSOA_TELEFONE
    {
        [Key]
        public int ID_PESSOA_TELEFONE { get; set; }

        public int? ID_PESSOA { get; set; }

        [StringLength(14)]
        public string NR_TELEFONE { get; set; }

        [StringLength(5)]
        public string CD_DDD { get; set; }

        [StringLength(1)]
        public string TIPO_TELEFONE { get; set; }

        [StringLength(1)]
        public string FL_TEL_PRINCIPAL { get; set; }

        public int? CD_OPERADORA_TELEFONIA { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        public virtual PESSOA PESSOA { get; set; }
    }
}
