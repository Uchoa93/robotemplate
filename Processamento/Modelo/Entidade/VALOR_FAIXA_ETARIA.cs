namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VALOR_FAIXA_ETARIA
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_VALOR_FAIXA_ETARIA { get; set; }

        public int IDADE_INICIAL { get; set; }

        public int IDADE_FINAL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal VALOR_OPERADORA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal VALOR_SUPERINTENDENCIA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ID_TABELA_VALORES { get; set; }

        public int ID_PLANO { get; set; }

        [Required]
        [StringLength(1)]
        public string STATUS { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DATA_ATUALIZACAO { get; set; }

        [StringLength(20)]
        public string USUARIO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string ESTACAO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string OBJETO_ATUALIZACAO { get; set; }

        public virtual PLANO PLANO { get; set; }

        public virtual TABELA_VALORES TABELA_VALORES { get; set; }
    }
}
