namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PARCELA_PARTICIPANTE
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_PARCELA_PARTICIPANTE { get; set; }

        public int ID_CONTRATO_PARTICIPANTE { get; set; }

        public int? ID_CONTRATO_PARCELA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal VALOR_PARCELA_INDIVIDUAL { get; set; }

        [Column(TypeName = "date")]
        public DateTime DATA_PROCESSAMENTO { get; set; }

        [Required]
        [StringLength(1)]
        public string STATUS { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DATA_ATUALIZACAO { get; set; }

        [StringLength(20)]
        public string USUARIO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string ESTACAO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string OBJETO_ATUALIZACAO { get; set; }
    }
}
