namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PAGTO_FORMA_PESSOA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PAGTO_FORMA_PESSOA()
        {
            CONTRATO = new HashSet<CONTRATO>();
        }

        [Key]
        public int ID_PAGTO_FORMA_PESSOA { get; set; }

        public int? ID_PESSOA { get; set; }

        public int? ID_PAGAMENTO_FORMA { get; set; }

        [StringLength(10)]
        public string IDENTIFICACAO_PESSOA_UNIDADE { get; set; }

        [StringLength(15)]
        public string IDENTIFICACAO_PESSOA_CONTA { get; set; }

        [StringLength(10)]
        public string IDENTIFICACAO_PESSOA_INSTITUICAO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTRATO> CONTRATO { get; set; }

        public virtual PAGAMENTO_FORMA PAGAMENTO_FORMA { get; set; }
    }
}
