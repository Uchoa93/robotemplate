namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PESSOA_FISICA
    {
        [Key]
        public int ID_PESSOA_FISICA { get; set; }

        public int ID_PESSOA { get; set; }

        [StringLength(50)]
        public string IDENTIDADE { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DT_EMISSAO_IDENTIDADE { get; set; }

        [StringLength(50)]
        public string DS_ORGAO_IDENTIDADE { get; set; }

        [StringLength(2)]
        public string ID_UF { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DT_NASCIMENTO { get; set; }

        [StringLength(1)]
        public string ID_ESTADO_CIVIL { get; set; }

        [StringLength(25)]
        public string CD_SEXO { get; set; }

        [StringLength(200)]
        public string NM_PAI { get; set; }

        [StringLength(200)]
        public string NM_MAE { get; set; }

        [StringLength(200)]
        public string NM_SOCIAL { get; set; }

        public virtual PESSOA PESSOA { get; set; }
    }
}
