namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TABELA_VALORES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TABELA_VALORES()
        {
            VALOR_FAIXA_ETARIA = new HashSet<VALOR_FAIXA_ETARIA>();
        }

        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_TABELA_VALORES { get; set; }

        [Column(TypeName = "date")]
        public DateTime DATA_INICIAL_VALIDADE { get; set; }

        [Column(TypeName = "date")]
        public DateTime DATA_FINAL_VALIDADE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ID_OPERADORA { get; set; }

        [StringLength(1)]
        public string STATUS { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DATA_ATUALIZACAO { get; set; }

        [StringLength(20)]
        public string USUARIO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string ESTACAO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string OBJETO_ATUALIZACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VALOR_FAIXA_ETARIA> VALOR_FAIXA_ETARIA { get; set; }
    }
}
