namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLANO_VALOR
    {
        [Key]
        public int ID_PLANO_VALOR { get; set; }

        public int ID_PLANO { get; set; }

        public int? IDADE_INICIO { get; set; }

        public int? IDADE_FIM { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_ADESAO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_MANUTENCAO { get; set; }

        [StringLength(1)]
        public string VALOR_LIVRE { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_LIVRE_MINIMO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_LIVRE_MAXIMO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        public virtual PLANO PLANO { get; set; }
    }
}
