namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MOVIMENTO")]
    public partial class MOVIMENTO
    {
        [Key]
        public int ID_MOVIMENTO { get; set; }

        public int? ID_CONTRATO { get; set; }

        public int? ID_CONTRATO_PARCELA { get; set; }

        public int? ID_EVENTO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR { get; set; }

        public DateTime? DATA_MOVIMENTO { get; set; }

        public DateTime? DATA_TRANSACAO { get; set; }

        [StringLength(1)]
        public string STATUS { get; set; }

        public int? ID_UNIDADE { get; set; }

        public int? ID_PAGAMENTO_TIPO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        public virtual CONTRATO CONTRATO { get; set; }

        public virtual CONTRATO_PARCELA CONTRATO_PARCELA { get; set; }

        public virtual EVENTO EVENTO { get; set; }

        public virtual PAGAMENTO_TIPO PAGAMENTO_TIPO { get; set; }

        public virtual UNIDADE UNIDADE { get; set; }
    }
}
