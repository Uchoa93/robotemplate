namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FERIADO")]
    public partial class FERIADO
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal id_feriado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? ano { get; set; }

        [Column(TypeName = "numeric")]
        public decimal mes { get; set; }

        [Column(TypeName = "numeric")]
        public decimal dia { get; set; }

        [Column(TypeName = "numeric")]
        public decimal tipo_feriado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? id_estado { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? id_municipio { get; set; }

        [StringLength(100)]
        public string descrição { get; set; }

        [Required]
        [StringLength(1)]
        public string situacao { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime atualizacao_data { get; set; }

        [StringLength(60)]
        public string atualizacao_estacao { get; set; }

        [StringLength(100)]
        public string atualizacao_objeto { get; set; }

        [StringLength(20)]
        public string atualizacao_usuario { get; set; }

        public virtual ESTADO ESTADO { get; set; }

        public virtual MUNICIPIO MUNICIPIO { get; set; }
    }
}
