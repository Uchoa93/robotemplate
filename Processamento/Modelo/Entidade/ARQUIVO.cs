namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ARQUIVO")]
    public partial class ARQUIVO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ARQUIVO()
        {
            REGISTRO = new HashSet<REGISTRO>();
        }

        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_ARQUIVO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal ID_INTEGRACAO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal CODIGO_INTEGRACAO { get; set; }

        public DateTime DATA_ARQUIVO { get; set; }

        [Required]
        [StringLength(100)]
        public string PASTA { get; set; }

        [Required]
        [StringLength(100)]
        public string NOME { get; set; }

        [Column(TypeName = "numeric")]
        public decimal QUANT_REGISTROS { get; set; }

        public decimal SITUACAO { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DATA_ATUALIZACAO { get; set; }

        [StringLength(20)]
        public string USUARIO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string ESTACAO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string OBJETO_ATUALIZACAO { get; set; }

        public int? ID_PROCESSAMENTO { get; set; }

        public int? CODIGO_PROCESSO { get; set; }

        public virtual INTEGRACAO INTEGRACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<REGISTRO> REGISTRO { get; set; }
    }
}
