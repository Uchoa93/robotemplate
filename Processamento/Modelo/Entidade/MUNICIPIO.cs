namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MUNICIPIO")]
    public partial class MUNICIPIO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MUNICIPIO()
        {
            FERIADO = new HashSet<FERIADO>();
        }

        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal id_municipio { get; set; }

        [Column(TypeName = "numeric")]
        public decimal id_estado { get; set; }

        [StringLength(50)]
        public string nome { get; set; }

        [Required]
        [StringLength(1)]
        public string situacao { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime atualizacao_data { get; set; }

        [StringLength(60)]
        public string atualizacao_estacao { get; set; }

        [StringLength(100)]
        public string atualizacao_objeto { get; set; }

        [StringLength(20)]
        public string atualizacao_usuario { get; set; }

        public virtual ESTADO ESTADO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FERIADO> FERIADO { get; set; }
    }
}
