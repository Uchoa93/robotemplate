namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ESTATUTO_IDOSO
    {
        [Key]
        public int id_estatuto_idoso { get; set; }

        public int id_contrato_participantes { get; set; }

        public int id_pessoa { get; set; }

        [Column(TypeName = "date")]
        public DateTime data_nascimento { get; set; }

        [Column(TypeName = "date")]
        public DateTime data_atual { get; set; }

        public DateTime data_atualizacao { get; set; }

        [StringLength(100)]
        public string nome_objeto { get; set; }
    }
}
