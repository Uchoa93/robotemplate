namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PAGAMENTO_TIPO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PAGAMENTO_TIPO()
        {
            MOVIMENTO = new HashSet<MOVIMENTO>();
            PAGAMENTO_FORMA = new HashSet<PAGAMENTO_FORMA>();
            PAGTO_FORMA_LAYOUT = new HashSet<PAGTO_FORMA_LAYOUT>();
        }

        [Key]
        public int ID_PAGAMENTO_TIPO { get; set; }

        [StringLength(200)]
        public string DESCRICAO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MOVIMENTO> MOVIMENTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PAGAMENTO_FORMA> PAGAMENTO_FORMA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PAGTO_FORMA_LAYOUT> PAGTO_FORMA_LAYOUT { get; set; }
    }
}
