namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PLANO_DIAS
    {
        [Key]
        public int ID_PLANO_DIAS { get; set; }

        public int? ID_PLANO { get; set; }

        public int? DIA_VENCTO { get; set; }

        [StringLength(1)]
        public string ACEITA_VENCTO { get; set; }

        public virtual PLANO PLANO { get; set; }
    }
}
