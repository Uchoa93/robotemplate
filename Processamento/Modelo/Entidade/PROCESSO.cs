namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PROCESSO")]
    public partial class PROCESSO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PROCESSO()
        {
            PROCESSAMENTO = new HashSet<PROCESSAMENTO>();
        }

        [Key]
        public int ID_PROCESSO { get; set; }

        public int CODIGO_PROCESSO { get; set; }

        [StringLength(100)]
        public string DESCRICAO { get; set; }

        [Required]
        [StringLength(100)]
        public string NOME_ROTINA { get; set; }

        public int? CODIGO_INTEGRACAO { get; set; }

        [StringLength(1)]
        public string TIPO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        public DateTime DATA_ATUALIZACAO { get; set; }

        [StringLength(50)]
        public string USUARIO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string OBJETO_ATUALIZACAO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PROCESSAMENTO> PROCESSAMENTO { get; set; }
    }
}
