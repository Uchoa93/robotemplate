namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PESSOA_RELACIONAMENTO
    {
        [Key]
        public int ID_PESSOA_RELACIONAMENTO { get; set; }

        public int? ID_PESSOA { get; set; }

        public int? ID_PESSOA_RELACIONADA { get; set; }

        [StringLength(1)]
        public string GRAU_PARENTESCO { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? INICIO_RELACIONAMENTO { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? FIM_RELACIONAMENTO { get; set; }

        [StringLength(1)]
        public string FL_DEPENDENTE { get; set; }

        [StringLength(1)]
        public string SITUACAO { get; set; }

        public virtual PESSOA PESSOA { get; set; }

        public virtual PESSOA PESSOA1 { get; set; }
    }
}
