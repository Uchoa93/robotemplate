namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PARAMETRO")]
    public partial class PARAMETRO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PARAMETRO()
        {
            PARAMETRO_VALOR = new HashSet<PARAMETRO_VALOR>();
        }

        [Key]
        public int id_parametro { get; set; }

        [Required]
        [StringLength(200)]
        public string descricao { get; set; }

        [Required]
        [StringLength(30)]
        public string chave { get; set; }

        [Required]
        [StringLength(1)]
        public string situacao { get; set; }

        public DateTime data_atualizacao { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PARAMETRO_VALOR> PARAMETRO_VALOR { get; set; }
    }
}
