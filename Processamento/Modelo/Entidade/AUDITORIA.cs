namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AUDITORIA")]
    public partial class AUDITORIA
    {
        [Key]
        public int ID_AUDITORIA { get; set; }

        [StringLength(100)]
        public string APP { get; set; }

        [Required]
        [StringLength(50)]
        public string DS_USUARIO_BD { get; set; }

        [Required]
        [StringLength(50)]
        public string DS_MAQUINA_BD { get; set; }

        public DateTime DT_OPERACAO { get; set; }

        [Required]
        [StringLength(60)]
        public string TABELA { get; set; }

        [Required]
        [StringLength(11)]
        public string OPERACAO { get; set; }

        [Column(TypeName = "xml")]
        public string DADO_ANTIGO { get; set; }

        [Column(TypeName = "xml")]
        public string DADO_NOVO { get; set; }

        [StringLength(50)]
        public string DS_USUARIO { get; set; }

        [StringLength(50)]
        public string IP_MAQUINA { get; set; }

        [StringLength(50)]
        public string DS_MAQUINA { get; set; }
    }
}
