namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class DATA_PROCESSAMENTO
    {
        [Key]
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal ID_DATA_PROCESSAMENTO { get; set; }

        [Column(TypeName = "date")]
        public DateTime DATA_ANTERIOR { get; set; }

        [Column(TypeName = "date")]
        public DateTime DATA_ATUAL { get; set; }

        [Column(TypeName = "date")]
        public DateTime DATA_POSTERIOR { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime DATA_ATUALIZACAO { get; set; }

        [Required]
        [StringLength(20)]
        public string USUARIO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string ESTACAO_ATUALIZACAO { get; set; }

        [StringLength(100)]
        public string OBJETO_ATUALIZACAO { get; set; }
    }
}
