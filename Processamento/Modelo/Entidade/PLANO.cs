namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PLANO")]
    public partial class PLANO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PLANO()
        {
            CONTRATO = new HashSet<CONTRATO>();
            PAGTO_FORMA_PLANO = new HashSet<PAGTO_FORMA_PLANO>();
            VALOR_FAIXA_ETARIA = new HashSet<VALOR_FAIXA_ETARIA>();
            PLANO_DIAS = new HashSet<PLANO_DIAS>();
            PLANO_VALOR = new HashSet<PLANO_VALOR>();
        }

        [Key]
        public int ID_PLANO { get; set; }

        public int? ID_OPERADORA { get; set; }

        [StringLength(50)]
        public string NOME { get; set; }

        [StringLength(200)]
        public string DESCRICAO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [StringLength(1)]
        public string SEGMENTO { get; set; }

        [StringLength(1)]
        public string RENOVA_AUTOMATICAMENTE { get; set; }

        public int? NUM_PARCELAS_ADESAO { get; set; }

        public int? NUM_PARCELAS_MANUTENCAO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MULTA_PERCENTUAL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? MORA_PERCENTUAL { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? PERCENTUAL_OPERADORA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTRATO> CONTRATO { get; set; }

        public virtual OPERADORA OPERADORA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PAGTO_FORMA_PLANO> PAGTO_FORMA_PLANO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VALOR_FAIXA_ETARIA> VALOR_FAIXA_ETARIA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLANO_DIAS> PLANO_DIAS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PLANO_VALOR> PLANO_VALOR { get; set; }
    }
}
