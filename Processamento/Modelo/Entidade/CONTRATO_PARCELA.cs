namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CONTRATO_PARCELA
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CONTRATO_PARCELA()
        {
            MOVIMENTO = new HashSet<MOVIMENTO>();
        }

        [Key]
        public int ID_CONTRATO_PARCELA { get; set; }

        public int? ID_CONTRATO { get; set; }

        public int? ID_PAGAMENTO_FORMA { get; set; }

        public int? NUMERO_PARCELA { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DATA_VENCIMENTO { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DATA_PAGAMENTO { get; set; }

        public DateTime? DATA_GERACAO { get; set; }

        [StringLength(1)]
        public string SITUACAO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_PARCELA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_PAGO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_PAGO_MULTA { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_PAGO_JUROS { get; set; }

        public virtual CONTRATO CONTRATO { get; set; }

        public virtual PAGAMENTO_FORMA PAGAMENTO_FORMA { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MOVIMENTO> MOVIMENTO { get; set; }

        internal bool isNullOrEmpty()
        {
            throw new NotImplementedException();
        }
    }
}
