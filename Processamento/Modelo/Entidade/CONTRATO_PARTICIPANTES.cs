namespace Processamento.Modelo.Entidade
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CONTRATO_PARTICIPANTES
    {
        [Key]
        public int ID_CONTRATO_PARTICIPANTES { get; set; }

        public int? ID_CONTRATO { get; set; }

        public int? ID_PESSOA { get; set; }

        [StringLength(1)]
        public string IDENTIFICACAO_OPERADOR { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DATA_INICIO { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? DATA_FIM { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_ADESAO { get; set; }

        [Column(TypeName = "numeric")]
        public decimal? VALOR_MANUTENCAO { get; set; }

        [Required]
        [StringLength(1)]
        public string SITUACAO { get; set; }

        [Required]
        [StringLength(1)]
        public string estatuto_idoso { get; set; }

        public virtual CONTRATO CONTRATO { get; set; }

        public virtual PESSOA PESSOA { get; set; }
    }
}
