using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Processamento.Modelo.Entidade;
using Processamento.VO;

namespace Processamento.Modelo.Negocio
{
	class ContratoParcela
	{
		private Contexto contexto;

		public ContratoParcela()
		{
			contexto = new Contexto();
		}

		public ContratoParcela(Contexto contexto)
		{
			this.contexto = contexto;
		}

		public void atualizar(CONTRATO_PARCELA o)
		{
			contexto.CONTRATO_PARCELA.Attach(o);
			contexto.Entry(o).State = EntityState.Modified;
			contexto.SaveChanges();
		}

		public CONTRATO_PARCELA obterPrimeiraPendente(Pagamento p)
		{
			return	contexto.CONTRATO_PARCELA.Where(
						o => "1".Equals(o.SITUACAO) 
						&& p.matricula.Equals(o.CONTRATO.PAGTO_FORMA_PESSOA.IDENTIFICACAO_PESSOA_CONTA) 
						&& o.CONTRATO.PAGTO_FORMA_PESSOA.PAGAMENTO_FORMA.ID_PAGAMENTO_FORMA == 15
					).FirstOrDefault();
		}

	}
}
