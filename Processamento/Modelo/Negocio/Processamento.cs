using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Processamento.Modelo.Entidade;

namespace Processamento.Modelo.Negocio
{
	class Processamento
	{
		private Contexto contexto;

		public Processamento()
		{
			contexto = new Contexto();
		}

		public Processamento(Contexto contexto)
		{
			this.contexto = contexto;
		}

		public int iniciar(int idProcesso, int idSuperintendencia, int idOperadora, DateTime DataAtual)
		{
			PROCESSAMENTO o = new PROCESSAMENTO();
            
			o.ID_OPERADORA = idOperadora;
			o.ID_PROCESSO = idProcesso;
			o.ID_SUPERINTENDENCIA = idSuperintendencia;
			o.SITUACAO = "I";
			o.DATA_INICIO = DateTime.Now;
			o.DATA_ATUALIZACAO = DateTime.Now;
			o.USUARIO_ATUALIZACAO = "SYSTEM";
			o.OBJETO_ATUALIZACAO = "Processamento.Modelo.Negocio.Processamento.iniciar";
            o.DATA_ATUAL = DataAtual.Date;

			contexto.PROCESSAMENTO.Add(o);
			contexto.SaveChanges();

			return o.ID_PROCESSAMENTO;
		}

		public int iniciar(int idProcesso, int idSuperintendencia, DateTime DataAtual)
		{
			PROCESSAMENTO o = new PROCESSAMENTO();
			//o.ID_OPERADORA = idOperadora;
			o.ID_PROCESSO = idProcesso;
			o.ID_SUPERINTENDENCIA = idSuperintendencia;
			o.SITUACAO = "I";
			o.DATA_INICIO = DateTime.Now;
			o.DATA_ATUALIZACAO = DateTime.Now;
			o.USUARIO_ATUALIZACAO = "SYSTEM";
			o.OBJETO_ATUALIZACAO = "Processamento.Modelo.Negocio.Processamento.iniciar";
            o.DATA_ATUAL = DataAtual.Date;

			contexto.PROCESSAMENTO.Add(o);
			contexto.SaveChanges();

			return o.ID_PROCESSAMENTO;
		}

		public void terminar(int id)
		{
			PROCESSAMENTO o = contexto.PROCESSAMENTO.Where(p => p.ID_PROCESSAMENTO == id).FirstOrDefault();
			o.SITUACAO = "T";
			o.DATA_FIM = DateTime.Now;

			contexto.PROCESSAMENTO.Attach(o);
			contexto.Entry(o).State = EntityState.Modified;
			contexto.SaveChanges();
		}

		public void erro(int id)
		{
			PROCESSAMENTO o = contexto.PROCESSAMENTO.Where(p => p.ID_PROCESSAMENTO == id).FirstOrDefault();
			o.SITUACAO = "E";
			o.DATA_FIM = DateTime.Now;

			contexto.PROCESSAMENTO.Attach(o);
			contexto.Entry(o).State = EntityState.Modified;
			contexto.SaveChanges();
		}
	}
}

