﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Processamento.Modelo.Entidade;

namespace Processamento.Modelo.Negocio
{
	class Registro
	{
		private Contexto contexto;

		public Registro()
		{
			contexto = new Contexto();
		}

		public Registro(Contexto contexto)
		{
			this.contexto = contexto;
		}

		public void salvar(REGISTRO o)
		{
			contexto.REGISTRO.Add(o);
			contexto.SaveChanges();
		}

	}
}
