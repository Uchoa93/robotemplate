﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Processamento.Modelo.Entidade;

namespace Processamento.Modelo.Negocio
{
	class Processo
	{
		private Contexto contexto;

		public Processo()
		{
			contexto = new Contexto();
		}

		public Processo(Contexto contexto)
		{
			this.contexto = contexto;
		}

		public bool jaProcessou(int codigoProcesso, DateTime dataAtual)
		{
			PROCESSO p = contexto.PROCESSO.Where(o => o.CODIGO_PROCESSO == codigoProcesso && "1".Equals(o.SITUACAO)).FirstOrDefault();
			return p.PROCESSAMENTO.Where(o => o.DATA_ATUAL == dataAtual && "T".Equals(o.SITUACAO)).Count() > 0;
		}

		public PROCESSO obterProcesso(int codigoIntegracao)
		{
			return contexto.PROCESSO.Where(o => o.CODIGO_INTEGRACAO == codigoIntegracao && "1".Equals(o.SITUACAO)).FirstOrDefault();
		}

		public PROCESSO obterProcesso(string nomeRotina)
		{
			return contexto.PROCESSO.Where(o => nomeRotina.Equals(o.NOME_ROTINA) && "1".Equals(o.SITUACAO)).FirstOrDefault();
		}

	}
}
