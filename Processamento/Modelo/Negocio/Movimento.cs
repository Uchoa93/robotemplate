﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Processamento.Modelo.Entidade;

namespace Processamento.Modelo.Negocio
{
	class Movimento
	{
		private Contexto contexto;

		public Movimento()
		{
			contexto = new Contexto();
		}

		public Movimento(Contexto contexto)
		{
			this.contexto = contexto;
		}

		public void salvar(MOVIMENTO o)
		{
			contexto.MOVIMENTO.Add(o);
			contexto.SaveChanges();
		}
	}
}
