﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Processamento.Modelo.Entidade;

namespace Processamento.Modelo.Negocio
{
	class Operadora
	{
		private Contexto contexto;
		
		public Operadora() {
			contexto = new Contexto();
		}

		public List<OPERADORA> obterOperadoras(string segmento)
		{
			return contexto.OPERADORA.Where(o => o.PLANO.Where(p => p.SEGMENTO.Equals(segmento)).Count() > 0).ToList<OPERADORA>();
		}
	}
}
