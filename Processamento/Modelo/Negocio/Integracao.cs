﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Processamento.Modelo.Entidade;

namespace Processamento.Modelo.Negocio
{
	class Integracao
	{
		private Contexto contexto;
		
		public Integracao() {
			contexto = new Contexto();
		}

		public Integracao(Contexto contexto)
		{
			this.contexto = contexto;
		}

		public INTEGRACAO obterPorId(int id)
		{
			return contexto.INTEGRACAO.Where(o => o.ID_INTEGRACAO == id).FirstOrDefault();
		}

		public INTEGRACAO obterPorCodigo(int codigo)
		{
			return contexto.INTEGRACAO.Where(o => o.CODIGO_INTEGRACAO == codigo && o.SITUACAO.Equals("A")).FirstOrDefault();
		}

	}
}
