﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Processamento.Modelo.Entidade;

namespace Processamento.Modelo.Negocio
{
	class Arquivo
	{
		private Contexto contexto;

		public Arquivo()
		{
			contexto = new Contexto();
		}

		public Arquivo(Contexto contexto)
		{
			this.contexto = contexto;
		}

		public decimal salvar(ARQUIVO o)
		{
			contexto.ARQUIVO.Add(o);
			contexto.SaveChanges();

			return o.ID_ARQUIVO;
		}

		public bool existe(ARQUIVO o)
		{
			return contexto.ARQUIVO.Where(item => o.NOME.Equals(item.NOME) && o.CODIGO_INTEGRACAO == item.CODIGO_INTEGRACAO).Count() > 0;
		}

		public bool existe(int codigoIntegracao, DateTime dataAtual)
		{
			return contexto.ARQUIVO.Where(item => codigoIntegracao == item.CODIGO_INTEGRACAO && dataAtual == item.DATA_ARQUIVO).Count() > 0;
		}
	}
}
