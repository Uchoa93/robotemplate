﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Processamento.Modelo.Entidade;

namespace Processamento.Modelo.Negocio
{
	class LogProcesso
	{
		private Contexto contexto;

		public LogProcesso() {
			contexto = new Contexto();
		}

		public LogProcesso(Contexto contexto)
		{
			this.contexto = contexto;
		}

		public void salvar(LOG_PROCESSO o)
		{
			contexto.LOG_PROCESSO.Add(o);
			contexto.SaveChanges();
		}

	}
}
