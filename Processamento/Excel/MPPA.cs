using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Processamento.Modelo.Negocio;
using Processamento.Modelo.Entidade;
using Processamento.VO;
using Processamento.Util;
using OfficeOpenXml;

namespace Processamento.Excel
{
	class MPPA
	{
		private const int CODIGO_INTEGRACAO = 970;
		private const int PRIMEIRA_LINHA = 6;
		private const int ID_EVENTO_PAGAMENTO = 12;
		private const int ID_PAGAMENTO_TIPO_FOLHA = 4;
		private const string SITUACAO_PARCELA_PAGA = "2";

		private Contexto contexto;
		public DateTime dataAtual { get; set; }

		public MPPA()
		{
			contexto = new Contexto();
		}

		public MPPA(Contexto contexto)
		{
			this.contexto = contexto;
		}

		public List<Pagamento> carregar()
		{
			Logger logger = new Logger();
			logger.nomeMetodo = "Processamento.Excel.MPPA.carregar";
			logger.dataAtual = dataAtual;

			List<Pagamento> lista = new List<Pagamento>();
			string pasta;
			string nome;
			int quantos;
			int inicio;
			int fim;
			decimal id;
			string origem;
			string destino;


			logger.info("Obtendo a integração.");
			INTEGRACAO i = (new Integracao()).obterPorCodigo(CODIGO_INTEGRACAO);

			if (i == null)
			{
				logger.info("A integração " + CODIGO_INTEGRACAO.ToString() + " não foi encontrada.");
				return lista;
			}
			else
			{
				logger.info("Integração: " + i.DESCRICAO);
			}

			int tamanho = i.PREFIXO.Length;

			logger.info("Obtendo a lista de arquivos do MPPA de " + i.PASTA);
			string pattern = i.PREFIXO + "*." + i.EXTENSAO;
			string[] arquivos = Directory.GetFiles(i.PASTA, pattern);

			if (arquivos.Count() <= 0) {
				logger.info("Nenhum arquivo do MPPA foi encontrado.");
				return lista;
			}

			Arquivo an = new Arquivo(contexto);
			Registro rn = new Registro(contexto);

			foreach (string arquivo in arquivos)
			{
				logger.info("Carregando o arquivo: " + arquivo);

				nome = Path.GetFileName(arquivo);
				pasta = Path.GetDirectoryName(arquivo);

				quantos = 0;

				using (ExcelPackage xl = new ExcelPackage(new FileInfo(arquivo))) 
				{
					ExcelWorksheet ws = xl.Workbook.Worksheets[1];

					for (int k = PRIMEIRA_LINHA; k <= ws.Dimension.End.Row; k++)
					{
						if (ws.Cells[k, 2].Value == null)
							break;

						Pagamento p = new Pagamento();

						p.nomeArquivo = nome;
						p.pasta = pasta;
						inicio = tamanho;
						fim = nome.IndexOf(i.EXTENSAO) - inicio - 1;
						string[] partes = nome.Substring(inicio, fim).Split(' ');
						p.mesNome = partes[0];
						p.mes = Mes.nomeMesParaInt(p.mesNome);
						p.ano = Convert.ToInt32(partes[1]);

						p.matricula = ws.Cells[k, 2].Value.ToString().Replace(".", "");
						p.nome = ws.Cells[k, 3].Value.ToString();
						p.valor = Convert.ToDecimal(ws.Cells[k, 12].Value);

						if (p.valor > 0)
						{
							lista.Add(p);

							quantos++;
						}
					}

					if (quantos > 0)
					{
						logger.info("Gravando o arquivo.");
						ARQUIVO a = new ARQUIVO();
						a.ID_INTEGRACAO = i.ID_INTEGRACAO;
						a.CODIGO_INTEGRACAO = i.CODIGO_INTEGRACAO;
						a.DATA_ARQUIVO = dataAtual;
						a.PASTA = pasta;
						a.NOME = nome;
						a.QUANT_REGISTROS = quantos;
						a.SITUACAO = 0;
						a.DATA_ATUALIZACAO = DateTime.Now;
						a.USUARIO_ATUALIZACAO = "SYSTEM";
						a.ESTACAO_ATUALIZACAO = "SERVER";
						a.OBJETO_ATUALIZACAO = logger.nomeMetodo;

						if (an.existe(a))
						{
							logger.info("O arquivo " + a.NOME + " já foi importado anteriormente.");
						}
						else
						{
							id = an.salvar(a);

							quantos = 0;

							logger.info("Gravando os registros.");

							foreach (Pagamento p in lista.Where(o => nome.Equals(o.nomeArquivo)))
							{
								REGISTRO r = new REGISTRO();
								r.ID_ARQUIVO = id;
								r.REGISTRO1 = p.matricula + ";" + p.nome + ";" + p.valor.ToString() + ";" + p.ano.ToString() + ";" + p.mes.ToString();
								r.SEQUENCIAL = ++quantos;
								r.ID_CONTRATO_PARCELA = 0;
								r.OBSERVACAO = "";
								r.SITUACAO = 1;
								r.DATA_ATUALIZACAO = DateTime.Now;
								r.USUARIO_ATUALIZACAO = a.USUARIO_ATUALIZACAO;
								r.ESTACAO_ATUALIZACAO = a.ESTACAO_ATUALIZACAO;
								r.OBJETO_ATUALIZACAO = a.OBJETO_ATUALIZACAO;

								rn.salvar(r);
							}

							logger.info("Registros: " + lista.Count.ToString());

							origem = i.PASTA + nome;
							destino = i.PASTA_PROCESSADOS + nome;
							File.Move(origem, destino);
						}

					}

				}

			}

			logger.info("Carga de arquivos MPPA finalizada.");

			return lista;
		}

		public void baixar(List<Pagamento> lista)
		{
			Logger logger = new Logger();
			logger.nomeMetodo = "Processamento.Excel.MPPA.baixar";
			logger.dataAtual = dataAtual;

			ContratoParcela cpn = new ContratoParcela(contexto);
			Movimento mn = new Movimento(contexto);
           
			DateTime hoje = DateTime.Today;

			logger.info("Baixando as parcelas.");

            foreach (Pagamento p in lista)
            {
                CONTRATO_PARCELA cp = cpn.obterPrimeiraPendente(p);
                if(cp == null)
                {
                    logger.info(""+p.nome +" "+p.matricula+" Não foi encontrada a parcela do mesmo");

                }
                else { 
                
                    cp.DATA_PAGAMENTO = dataAtual;
                    cp.VALOR_PAGO = p.valor;
                    cp.SITUACAO = SITUACAO_PARCELA_PAGA;

                    cpn.atualizar(cp);


                    MOVIMENTO m = new MOVIMENTO();

                    m.ID_CONTRATO = cp.ID_CONTRATO;
                    m.ID_CONTRATO_PARCELA = cp.ID_CONTRATO_PARCELA;
                    m.ID_EVENTO = ID_EVENTO_PAGAMENTO;
                    m.VALOR = p.valor;
                    m.DATA_MOVIMENTO = hoje;
                    m.DATA_TRANSACAO = hoje;
                    m.STATUS = "E";
                    m.SITUACAO = "E";
                    m.ID_PAGAMENTO_TIPO = ID_PAGAMENTO_TIPO_FOLHA;

                    mn.salvar(m);
                }
			}

			logger.info("Baixa de parcelas finalizada.");
		}
	}
}
